package com.springboot.testing.servicesimpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.testing.dao.IClienteDao;
import com.springboot.testing.entity.Cliente;
import com.springboot.testing.services.IClienteService;

@Service
public class ClienteService implements IClienteService{
	
	private static final Logger LOG = LoggerFactory.getLogger(ClienteService.class);

	
	@Autowired
	private IClienteDao clienteDao;

	@Override
	@Transactional(readOnly = true)
	public Cliente findById(Long id) {
		return clienteDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAll() {
		return (List<Cliente>) clienteDao.findAll();
	}

	@Override
	@Transactional
	public Cliente save(Cliente cliente) {
		Cliente clienteSave = new Cliente();
		clienteSave = clienteDao.save(cliente);
		LOG.info("El cliente: ".concat(cliente.getNombre().concat(" ").concat(cliente.getApellido()).concat(" se a guardado correctamente")));
		return clienteSave;
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		clienteDao.deleteById(id);
		LOG.info("Borrado satisfactoriamente!");
	}

}
