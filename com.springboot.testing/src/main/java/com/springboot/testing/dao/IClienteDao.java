package com.springboot.testing.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.testing.entity.Cliente;


public interface IClienteDao extends CrudRepository<Cliente,Long>{

}
