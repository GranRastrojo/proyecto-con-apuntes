package com.springboot.testing.services;

import java.util.List;

import com.springboot.testing.entity.Cliente;

public interface IClienteService {
	
	public Cliente findById(Long id);
	
	public List<Cliente> findAll();
	
	public Cliente save(Cliente cliente);
	
	public void deleteById(Long id);

}
